const router = require("express").Router();
const { celebrate, Joi, errors, Segments } = require('celebrate');
const helloController = require("../controllers/hello.controller");

router.get("/", celebrate({
    [Segments.QUERY]: Joi.object().keys({
        name: Joi.string().optional()
    })
}), helloController.helloworld);

router.use(errors());
module.exports = router;