module.exports = app => {
    const express = require("express");
    const cors = require("cors");
    const morgan = require('morgan');
    const chalk = require('chalk');
    const moment = require('moment-timezone');
    const { handleSystaxError, handleNotFoundError, handleErrorTimeOut } = require('../helpers/handleerror');
    const { headerSecurity } = require('../helpers/header');
    const { logResponseBody, loggingBody } = require('../middlewares/logging');

    //header security
    app.use(headerSecurity);

    let corsOptions = {
        origin: '*'
    };
    //cross site
    app.use(cors(corsOptions));

    // parse requests of content-type - application/json
    app.use(express.urlencoded({ limit: '80mb', extended: true }));
    app.use(express.raw());
    // parse application/json
    app.use(express.json({ limit: '80mb', extended: true }));

    //logging
    app.use(loggingBody)

    app.use(morgan(function (tokens, req, res) {
        // console.log(res['__loggingbody__'])
        return [
            chalk.hex('#3498DB').bold(`[${moment(tokens.date(req, res)).tz('Asia/Bangkok').format('YYYY/MM/DD HH:mm:ss')}]`),
            chalk.hex('#E59866').bold(tokens['remote-addr'](req, res)),
            chalk.hex('#ff4757').bold('- -'),
            chalk.hex('#D4AC0D').bold(tokens.method(req, res)),
            chalk.hex('#3498DB').bold(tokens.url(req, res)),
            chalk.hex(tokens.status(req, res) === '200' ? '#16A085' : '#CB4335').bold(tokens.status(req, res)),
            //tokens.res(req, res, 'content-length'),
            //tokens['user-agent'](req, res),
            chalk.hex('#ff4757').bold('- -'),
            chalk.hex('#16A085').bold(tokens['response-time'](req, res)),
            chalk.hex('#16A085').bold('ms')
        ].join(' ')
    }))

    app.use(morgan(function (tokens, req, res) {
        return [
            chalk.hex('#16A085').bold(`Header:`),
            `${chalk.hex('#F4D03F').bold('[')}${JSON.stringify(req.headers)}${chalk.hex('#F4D03F').bold(']')}`,
        ].join(' ')
    }))

    app.use(morgan(function (tokens, req, res) {
        return [
            chalk.hex('#16A085').bold(`Reqeust:`),
            `${chalk.hex('#F4D03F').bold('[')}${JSON.stringify(req.body)}${chalk.hex('#F4D03F').bold(']')}`,
        ].join(' ')
    }))

    app.use(morgan(function (tokens, req, res) {
        return [
            chalk.hex('#16A085').bold(`Response:`),
            `${chalk.hex('#F4D03F').bold('[')}${res['__loggingbody__']}${chalk.hex('#F4D03F').bold(']')}`,
        ].join(' ')
    }))

    app.use(handleSystaxError);
    app.use(handleNotFoundError);
    app.use(handleErrorTimeOut);
}