const { createLogger, format, transports } = require('winston');
// const Buffer = require('buffer')

//logging
const logger = createLogger({
  level: 'debug',
  format: format.combine(
    format.colorize(),
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [new transports.Console()]
});

function logResponseBody(req, res, next) {
  var oldWrite = res.write,
    oldEnd = res.end;

  var chunks = [];

  res.write = function (chunk) {
    // eslint-disable-next-line
    chunks.push(Buffer.from(chunk));
    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk)
      // eslint-disable-next-line
      chunks.push(Buffer.from(chunk));
    // eslint-disable-next-line
    var body = Buffer.concat(chunks).toString('utf8');
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var ua = req.headers['user-agent'];
    try {
      console.log('start')
      console.log('.')
      console.log('.')
      logger.info(`${req.method} ${req.originalUrl} ${ip} ${ua}`);
      console.log('--------------------------------------------------------------------------')
      logger.info(`header:${JSON.stringify(req.headers)}`);
      console.log('--------------------------------------------------------------------------')
      logger.info(`request:${JSON.stringify(req.body)}`);
      console.log('-------------------------------------------------------------------------------')
      logger.info(`response:${body}`);
      console.log('.')
      console.log('.')
      console.log('.')
      console.log('end')
    } catch (err) {
      logger.error(err);
    }

    oldEnd.apply(res, arguments);
  };

  next();
}

const loggingBody = (req, res, next) => {
  const oldWrite = res.write,
    oldEnd = res.end,
    chunks = [];

  res.write = function (chunk) {
    chunks.push(Buffer.from(chunk));
    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk) {
      chunks.push(Buffer.from(chunk));
    }
    const body = Buffer.concat(chunks).toString('utf8');
    res.__loggingbody__ = body;
    oldEnd.apply(res, arguments);
  };
  next();
}

module.exports = {
  logResponseBody,
  loggingBody
}