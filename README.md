# Startup Project NodeJs

Project services

```
#dowload project to local

$ git clone url_repository
$ cd services
$ npm install
```

## Setup env and run start project

```
$ npm start

#or
    
$ node index.js
```

## Database Migration

create model and migration file

```
npx sequelize-cli model:generate --name User --attributes firstname:String
```

run migrate
```
npm run db:migrate
```

create seeder
```
npx sequelize-cli seed:generate --name demo-user
```

run seeder
```
npm run db:seeds
```

## Runtesting Unit Test
    
```
$ npm test 
```

## Running Server PM2
[Document](https://pm2.keymetrics.io/)
### pm2 start 

```
# start default env dev
$ pm2 start index.js --name services -- --color

# option start env production
$ NODE_ENV=production pm2 start index.js --name services -- --color
```

### pm2 log


### pm2 monit

## Managing processes

```
$ pm2 restart app_name
$ pm2 reload app_name
$ pm2 stop app_name
$ pm2 delete app_name
```

## Set up Project by docker
[Document](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)
```
$ cd services
$ docker build . -t services

#docker image list
$ docker images

# run docker port 4001 -> 4001
$ docker run -p 4001:4001 -d services

```
* enjoy!

## License

[OTG](http://www.8interactive.co.th/)