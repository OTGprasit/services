const express = require("express");
const app = express();
const dotenv = require('dotenv');
dotenv.config({
    path: process.env.NODE_ENV ? '.env' : '.env.example'
});

//set routes
require('./routes')(app);

//connect db
const db = require("./models");
db.sequelize.sync();

// set port, listen for requests
// eslint-disable-next-line
const PORT = process.env.PORT;
app.listen(PORT, () => {
    // eslint-disable-next-line
    console.log(`Server ${process.env.NODE_ENV} is running on port ${PORT}.`);
});