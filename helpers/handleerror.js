const handleSystaxError = (err, req, res, next) => {  
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        console.error(err);
        res.status(400).json({
            code:err.status ,
            message:'SyntaxError',
            isSuccess:false
        });
    } else {
        next();
    }
}

const handleNotFoundError = (req, res, next) => {
    if(req.accepts('html') && res.status(404)) {
        res.status(404).json({
            code:404 ,
            message:'Not Found Page',
            isSuccess:false
        });
    } else {
        next();
    }
}

const handleErrorTimeOut = (err, req, res, next) => {
    if(res.status(504)) {
        res.status(504).json({
            code:504 ,
            message:'Error Time Out',
            isSuccess:false
        });
    } else {
        next();
    }
}

module.exports = {
    handleSystaxError,
    handleNotFoundError,
    handleErrorTimeOut
}