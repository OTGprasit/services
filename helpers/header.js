const headerSecurity = (req, res, next) => {
    res.setHeader("Content-Security-Policy", "script-src 'self' *");
    // res.setHeader("X-Frame-Options", "SAMEORIGIN");
    // res.setHeader("X-content-type-options", "nosniff");
    // res.setHeader("Referrer-Policy", "no-referrer");
    // res.setHeader("permitted-cross-domain-policies", "none");
    // res.setHeader("Permissions-Policy", "geolocation=(),midi=(),sync-xhr=(),microphone=(),camera=(),magnetometer=(),gyroscope=(),fullscreen=(self),payment=()")
    // res.setHeader("Expect-CT", "enforce, max-age=86400, report-uri='https://ttbwebportal-test.allianz.co.th/'");
    res.setHeader("X-Powered-By", "Octagon Interactive");
    return next();
}

module.exports = {
    headerSecurity
}